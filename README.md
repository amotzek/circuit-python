## Cooperative Multitasking

Im Verzeichnis src/main/python/Libraries findet sich die Bibliothek
cooperativemultitasking.py. Die daraus erzeugte Datei cooperativemultitasking.mpy
kann im Bereich Downloads heruntergeladen werden. Die Datei ist mit mpy-cross für
Circuit Python 3 erstellt.

### Beispiel

Das Beispiel verwendet die Klasse Tasks, um die Built-in LED blinken zu lassen.

    from cooperativemultitasking import Tasks
    from digitalio import DigitalInOut, Direction
    import board

    tasks = Tasks()
    led = DigitalInOut(board.D13)
    led.direction = Direction.OUTPUT

    def on():
        led.value = True
        tasks.after(1000, off)  # after 1000 milliseconds call off()

    def off():
        led.value = False
        tasks.after(1000, on)  # after 1000 milliseconds call on()

    tasks.now(on)  # call on() now

    while True:
        tasks.run()


### Details

cooperativemultitasking ist eine Bibliothek, mit der mehrere Funktionen (fast)
gleichzeitig oder unabhängig voneinander ausgeführt werden können. Um das zu
ermöglichen, verwalten Instanzen der Klasse `Tasks` Listen von Aufgaben.

Aufgaben können mit den Methoden `now`, `after`, `await_then` und `await_for_then`
in die Liste aufgenommen werden:

* Mit `now(f)` wird der Aufruf der Funktion `f` an den Anfang der Liste
  gestellt. Der Aufruf von `f` findet dann sofort statt.
* Mit `after(1000, f)` findet der Aufruf der Funktion `f` erst in 1000
  Millisekunden statt.
* Mit `await_then(g, f)` findet der Aufruf der Funktion `f` statt, sobald die
  Funktion `g` den Wert true liefert.
* Mit `await_for_then(g, 300, f)` findet der Aufruf der Funktion `f` statt,
  wenn `g` mindestens 300 Millisekunden lang den Wert `true` geliefert hat.

Mit der Methode `only_one_of` kann man deklarieren, dass nur eine von mehreren
Aufgaben ausgeführt werden soll - die, die als erste aktiv wird.
