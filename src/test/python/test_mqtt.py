import unittest
import socket
import mqtt

class TestCase(unittest.TestCase):

  def runTest(self):
      hostname = 'broker.hivemq.com'
      client_id = ''
      user_name = ''
      password = ''
      topic = 'tuple/:test'
      payload = 'lololooOO'
      #
      address = (hostname, 1883)
      broker = socket.socket()
      broker.connect(address)
      stream = broker.makefile(mode = 'rwb')
      #
      request = mqtt.ConnectRequest(client_id, user_name, password)
      request.write_to(stream)
      stream.flush()
      response = mqtt.AbstractResponse.receive_from(stream)
      #
      if not response.connection_accepted():
          raise ValueError('cannot connect')
      #
      request = mqtt.SubscribeRequest(1, topic)
      request.write_to(stream)
      stream.flush()
      response = mqtt.AbstractResponse.receive_from(stream)
      #
      if not response.has_packet_id(1):
          raise ValueError('wrong packet id ' + str(response.packet_id))
      #
      if not response.subscription_accepted():
          raise ValueError('cannot subscribe ' + str(response.return_code))
      #
      request = mqtt.PublishRequest(topic, payload)
      request.write_to(stream)
      stream.flush()
      #
      response = mqtt.AbstractResponse.receive_from(stream)
      #
      if not response.has_topic(topic):
        raise ValueError('unexpected topic ' + response.topic)
      #
      broker.close()
