import sys

sys.path.append('.')
sys.path.append('../../../test/python')

import unittest
import test_mqtt

suite = unittest.TestSuite()
suite.addTest(test_mqtt.TestCase())
testresult = None

runner = unittest.TextTestRunner()
testresult = runner.run(suite)

if not testresult.wasSuccessful():
  exit(1)
