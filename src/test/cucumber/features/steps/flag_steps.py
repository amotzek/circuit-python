import time

@given('I clear the flag')
def clear_flag(context):
  context.my_flag = False

@given('I set the flag')
@when('I set the flag')
def set_flag(context):
  context.my_flag = True
