from behave import *
from cooperativemultitasking import Tasks

@given('I have no tasks')
def no_tasks(context):
  context.my_tasks = Tasks()

@when('I add a task to append {elem} now')
def append_element_task_now(context, elem):
  context.my_tasks.now(lambda : context.my_list.append(elem))

@when('I add a task to append {elem} after {duration:n} milliseconds')
def append_element_task_after(context, elem, duration):
  context.my_tasks.after(duration, lambda : context.my_list.append(elem))

@when('I add a task to append {elem} if the flag is set')
def append_element_task_if_flag(context, elem):
  context.my_tasks.if_then(lambda : context.my_flag, lambda : context.my_list.append(elem))

@when('I execute all tasks')
def execute_all_tasks(context):
  while context.my_tasks.available():
    context.my_tasks.run()

@when('I execute at most one task')
def execute_at_most_one_task(context):
  context.my_tasks.run()

@then('I still have one or more tasks')
def has_tasks(context):
  assert context.my_tasks.available(), 'there should be a task'


