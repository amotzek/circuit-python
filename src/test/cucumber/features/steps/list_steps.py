from behave import *

@given('I have an empty list')
def empty_list(context):
  context.my_list = []

@then('I still have an empty list')
def one_element_list(context):
  assert len(context.my_list) == 0, f'list {context.my_list} should have no elements'

@then('I have a list with 1 element')
def one_element_list(context):
  assert len(context.my_list) == 1, f'list {context.my_list} should have one element'

@then('I have a list with {count:n} elements') # :n bedeutet, dass eine Zahl erwartet wird, siehe https://www.python.org/dev/peps/pep-3101/#format-strings
def more_element_list(context, count):
  assert len(context.my_list) == count, f'list {context.my_list} should have {count} elements'

@then('I have a list that contains {elem}')
def element_in_list(context, elem):
  assert elem in context.my_list, f'list {context.my_list} should contain {elem}'

@then('I have a list with {elem} at position {pos:n}')
def element_at_position(context, elem, pos):
  assert elem == context.my_list[pos], f'list {context.my_list} should have {elem} at position {pos}'
