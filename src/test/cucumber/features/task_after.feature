Feature: Execute tasks with delay

  Background:
      Given I have an empty list
        and I have no tasks
        and I start the clock

  Scenario: Execute one task
       When I add a task to append "a" after 100 milliseconds
        and I execute all tasks
       then I have a list with 1 element
        and I have a list that contains "a"
        and at least 100 milliseconds are elapsed

  Scenario: Execute four tasks in order
       When I add a task to append "a" after 90 milliseconds
        and I add a task to append "b" after 30 milliseconds
        and I add a task to append "c" after 120 milliseconds
        and I add a task to append "d" after 60 milliseconds
        and I execute all tasks
       then I have a list with 4 elements
        and I have a list with "b" at position 0
        and I have a list with "d" at position 1
        and I have a list with "a" at position 2
        and I have a list with "c" at position 3
        and at least 120 milliseconds are elapsed
