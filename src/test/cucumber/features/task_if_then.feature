Feature: Execute tasks with conditions

  Background:
      Given I have an empty list
        and I have no tasks

  Scenario: Execute no task
      Given I clear the flag
       When I add a task to append "a" if the flag is set
        and I execute at most one task
       then I still have an empty list
        and I still have one or more tasks

  Scenario: Execute one task
      Given I set the flag
       When I add a task to append "a" if the flag is set
        and I execute at most one task
       then I have a list with 1 element
        and I have a list that contains "a"

  Scenario: Execute one task after setting the flag
      Given I clear the flag
       When I add a task to append "a" if the flag is set
        and I execute at most one task
       then I still have an empty list
        and I still have one or more tasks
       When I set the flag
        and I execute all tasks
       then I have a list with 1 element
        and I have a list that contains "a"

  Scenario: Execute one task after a delay and then another task after setting the flag
      Given I clear the flag
        and I start the clock
       When I add a task to append "a" if the flag is set
        and I add a task to append "b" after 30 milliseconds
        and I execute at most one task
        and I execute at most one task
       then I have a list with 1 element
        and I have a list that contains "b"
        and I still have one or more tasks
        and at least 30 milliseconds are elapsed
       When I set the flag
        and I execute all tasks
       then I have a list with 2 elements
        and I have a list with "b" at position 0
        and I have a list with "a" at position 1
