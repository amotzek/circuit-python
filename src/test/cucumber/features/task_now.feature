Feature: Execute tasks immediately

  Background:
      Given I have an empty list
        and I have no tasks

  Scenario: Execute one task
       When I add a task to append "a" now
        and I execute all tasks
       then I have a list with 1 element
        and I have a list that contains "a"

  Scenario: Execute three tasks
       When I add a task to append "a" now
        and I add a task to append "b" now
        and I add a task to append "c" now
        and I execute all tasks
       then I have a list with 3 elements
        and I have a list that contains "a"
        and I have a list that contains "b"
        and I have a list that contains "c"
