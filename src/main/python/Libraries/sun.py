from math import pi, sin, cos, tan, atan2, asin, floor

def to_radiant(d):
    d %= 360.0
    return pi * d / 180.0

def to_degrees(r):
    d = 180.0 / pi * r
    d %= 360.0
    return d

class Sun:

    def __init__(self, latitude, longitude):
        self.latitude = to_radiant(latitude)
        self.longitude = to_radiant(longitude)

    @staticmethod
    def _declination(e, l):
        return asin(sin(e) * sin(l))

    def _azimuth(self, t, d):
        return atan2(cos(t) * sin(self.latitude) - tan(d) * cos(self.latitude), sin(t))

    def _altitude(self, t, d):
        return asin(cos(d) * cos(t) * cos(self.latitude) + sin(d) * sin(self.latitude))

    def position(self, n):  # n ist die Anzahl der Tage seit dem 1.1.2018 00:00 UTC
        n += 6574.5
        g = to_radiant(357.528 + 0.9856003 * n)  # mittlere Anomalie
        l = to_radiant(280.46 + 0.9856474 * n)  # mittlere ekliptikale Laenge
        l += to_radiant(1.915) * sin(g)
        l += to_radiant(0.01997) * sin(2.0 * g)  # ekliptikale Laenge
        e = to_radiant(23.439 - 0.0000004 * n)  # Schiefe der Ekliptik
        d = self._declination(e, l)  # Deklination
        t = to_radiant(360.0 * (n % 24.0)) + self.longitude  # Stundenwinkel
        a = self._azimuth(t, d)  # Azimut
        h = self._altitude(t, d)  # Hoehe
        if h < 0.0:
            return None
        return (to_degrees(a), to_degrees(h))