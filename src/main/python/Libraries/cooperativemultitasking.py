# Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
#
# This file is part of the Cooperative Multitasking package.
#
# You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
# See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
#
# This file is distributed in the hope that it will be useful, but without any warranty; without even
# the implied warranty of merchantability or fitness for a particular purpose.

import time
import gc

class Task:
    def __init__(self, when, priority, continuation, guard = None, duration = 0):
        self.when = when
        self.priority = priority
        self.continuation = continuation
        self.guard = guard
        self.siblings = []
        if not guard is None:
            self.duration = duration
            self.remaining = duration

class Tasks:
    def __init__(self):
        self.heap = [None]
        self.count = 0
        self.cycle = 30 # Millisekunden
        self.current = int(1000.0 * time.monotonic())

    def now(self, continuation, priority = 0):
        task = Task(self.current, priority, continuation)
        self._add(task)
        return task

    def after(self, duration, continuation, priority = 0):
        task = Task(self.current + int(duration), priority, continuation)
        self._add(task)
        return task

    def await_for_then(self, guard, duration, continuation, priority = 0):
        task = Task(self.current, priority, continuation, guard, int(duration))
        self._add(task)
        return task

    def await_then(self, guard, continuation, priority = 0):
        return self.await_for_then(guard, 0, continuation, priority)

    def if_for_then(self, guard, duration, continuation, priority = 0):  # deprecated
        return self.await_for_then(guard, duration, continuation, priority)

    def if_then(self, guard, continuation, priority = 0):  # deprecated
        return self.await_then(guard, continuation, priority)

    def only_one_of(self, task1, task2, task3 = None, task4 = None):
        if task4 is None and task3 is None:
            task1.siblings = [task1, task2]
            task2.siblings = task1.siblings
        elif task4 is None:
            task1.siblings = [task1, task2, task3]
            task2.siblings = task1.siblings
            task3.siblings = task1.siblings
        else:
            task1.siblings = [task1, task2, task3, task4]
            task2.siblings = task1.siblings
            task3.siblings = task1.siblings
            task4.siblings = task1.siblings

    def available(self):
        return self.count > 0

    def clear(self):
        self.heap = [None]
        self.count = 0

    def run(self):
        task = self._extract(1)
        if task is None:
            time.sleep(float(self.cycle) / 1000.0)
            self.current += self.cycle
            return
        self.current = max(self.current, int(1000.0 * time.monotonic()))
        if self.current < task.when:
            gc.collect()
            time.sleep(float(task.when - self.current) / 1000.0)
            self.current = task.when
        if not task.guard is None:
            result = task.guard()
            if result:
                if task.remaining > self.cycle:
                    task.remaining -= self.cycle
                    task.when = self.current + self.cycle
                    self._add(task)
                    return
            else:
                task.remaining = task.duration
                task.when = self.current + self.cycle
                self._add(task)
                return
        for sibling in task.siblings:
            if not task is sibling:
                self._remove(sibling)
        task.continuation()

    def _add(self, task):
        self.count += 1
        if len(self.heap) == self.count:
            self.heap.append(task)
        else:
            self.heap[self.count] = task
        self._bottom_up(self.count)

    def _extract(self, i):
        if self._is_outside(i):
            return None
        task = self.heap[i]
        self._top_down(i)
        return task

    def _remove(self, task):
        i = self._find(task)
        if self._is_inside(i):
            self._top_down(i)

    def _find(self, task, i = 1):
        while not self._is_leaf(i):
            if self.heap[i] is task:
              return i
            if self._is_before(task, self.heap[i]):
              return 0
            r = self._right_child(i)
            if self._is_inside(r):
                j = self._find(task, r)
                if self._is_inside(j):
                    return j
            i = self._left_child(i)
        if self.heap[i] is task:
            return i
        return 0

    def _bottom_up(self, i):
        while self._has_parent(i):
            p = self._parent(i)
            if self._is_before(self.heap[i], self.heap[p]):
                task = self.heap[i]
                self.heap[i] = self.heap[p]
                self.heap[p] = task
                i = p
            else:
                return

    def _top_down(self, i):
        while not self._is_leaf(i):
            l = self._left_child(i)
            r = self._right_child(i)
            if self._is_inside(r) and self._is_before(self.heap[r], self.heap[l]):
                self.heap[i] = self.heap[r]
                i = r
            else:
                self.heap[i] = self.heap[l]
                i = l
        if i == self.count:
            self.heap[i] = None
            self.count -= 1
        else:
            self.heap[i] = self.heap[self.count]
            self.heap[self.count] = None
            self.count -= 1
            self._bottom_up(i)

    @staticmethod
    def _is_before(task1, task2):
        if task1.when < task2.when:
            return True
        if task1.when == task2.when and task1.priority > task2.priority:
            return True
        return False

    def _is_outside(self, i):
        return i < 1 or i > self.count

    def _is_inside(self, i):
        return i >= 1 and i <= self.count

    def _is_leaf(self, i):
        return i > (self.count >> 1)

    @staticmethod
    def _has_parent(i):
        return i > 1

    @staticmethod
    def _parent(i):
        return i >> 1

    @staticmethod
    def _left_child(i):
        return i << 1

    @staticmethod
    def _right_child(i):
        return (i << 1) + 1