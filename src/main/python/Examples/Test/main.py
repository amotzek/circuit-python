import random
from cooperativemultitasking import Tasks
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction
from neopixel import NeoPixel
import board

tasks = Tasks()
lightsensor = AnalogIn(board.LIGHT)
led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT
pixels = NeoPixel(board.NEOPIXEL, 10, brightness = 0.2)
pixels.fill((0, 0, 0))
pixels.show()
colors = [(0x10, 0, 0), (0, 0x10, 0), (0, 0, 0x10),
          (0x10, 0x10, 0), (0x10, 0, 0x10), (0, 0x10, 0x10)]
pixelcount = 0
position = 0

def light():
    led.value = False
    tasks.if_for_then(is_dark, 5000, switch_on_led)  # if is_dark() for 5 seconds then call switch_on_led()

def is_dark():
    return lightsensor.value < 7000

def switch_on_led():
    led.value = True
    tasks.after(1000, switch_off_led)  # after 1 second call switch_off_led()

def switch_off_led():
    led.value = False
    task1 = tasks.after(1000, switch_on_led)  # after 1 second call switch_on_led()
    task2 = tasks.if_then(is_light, light)  # if is_light() then call light()
    tasks.only_one_of(task1, task2)  # do either task1 or task2

def is_light():
    return lightsensor.value > 9000

def rotate_pixels():
    first = pixels[0]
    for i in range(1, len(pixels)):
        pixels[i - 1] = pixels[i]
    pixels[len(pixels) - 1] = first
    pixels.show()
    global position
    position -= 1
    position %= len(pixels)
    tasks.after(300, rotate_pixels)  # after 300 milliseconds call rotate_pixels() again

def blink_pixel(i):

    def switch_on_pixel():
        pixels[(i + position) % len(pixels)] = colors[random.randint(0, len(colors) - 1)]
        tasks.after(random.randint(4000, 8000), switch_off_pixel)  # after 4 to 8 seconds call switch_off_pixel()

    def switch_off_pixel():
        pixels[(i + position) % len(pixels)] = (0, 0, 0)
        global pixelcount
        pixelcount -= 1

    tasks.after(random.randint(0, 1000), switch_on_pixel)  # after up to 1 second call switch_on_pixel()

def blink_pixels():
    indexes = []
    global pixelcount
    pixelcount = random.randint(1, 5)
    while len(indexes) < pixelcount:
        i = random.randint(0, len(pixels) - 1)
        if not i in indexes:
            indexes.append(i)
            blink_pixel(i)
    tasks.if_then(no_more_pixels, blink_pixels)  # if all pixels are off call blink_pixels() again

def no_more_pixels():
    return pixelcount == 0

tasks.now(switch_off_led)  # call switch_off_led() now
tasks.now(rotate_pixels)  # call rotate_pixels() now
tasks.now(blink_pixels)  # call blink_pixels() now

while True:
    tasks.run()