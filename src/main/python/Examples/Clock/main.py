import random
from cooperativemultitasking import Tasks
from neopixel import NeoPixel
from busio import I2C
from adafruit_pcf8523 import PCF8523
import board
import gc

tasks = Tasks()
pixels = NeoPixel(board.NEOPIXEL, 10, brightness = 0.2)
pixels.fill((0, 0, 0))
pixels.show()
colors = [(0x10, 0, 0), (0, 0x10, 0), (0, 0, 0x10),
          (0x10, 0x10, 0), (0x10, 0, 0x10), (0, 0x10, 0x10)]
pixelcount = 0
position = 0
i2c = I2C(board.SCL, board.SDA)
rtc = PCF8523(i2c)
hour = 0
minute = 0

def read_datetime():
    datetime = rtc.datetime
    global hour, minute
    hour = datetime.tm_hour
    minute = datetime.tm_min
    tasks.after(60000, read_datetime)  # after 1 minute call read_datetime() again

def rotate_pixels():
    first = pixels[0]
    for i in range(1, len(pixels)):
        pixels[i - 1] = pixels[i]
    pixels[len(pixels) - 1] = first
    pixels.show()
    global position
    position -= 1
    position %= len(pixels)
    if pixelcount > 0:
        tasks.after(333, rotate_pixels)  # after 333 milliseconds call rotate_pixels() again

def blink_pixel(i):

    def switch_on_pixel():
        pixels[(i + position) % len(pixels)] = colors[random.randint(0, len(colors) - 1)]
        tasks.after(random.randint(8000, 9000), switch_off_pixel)  # after 8 to 9 seconds call switch_off_pixel()

    def switch_off_pixel():
        pixels[(i + position) % len(pixels)] = (0, 0, 0)
        global pixelcount
        pixelcount -= 1

    tasks.after(random.randint(0, 1000), switch_on_pixel)  # after up to 1 second call switch_on_pixel()

def show_minutes():
    indexes = []
    global pixelcount
    pixelcount = int(minute / 15) + 1
    while len(indexes) < pixelcount:
        i = random.randint(0, len(pixels) - 1)
        if not i in indexes:
            indexes.append(i)
            blink_pixel(i)
    tasks.now(rotate_pixels)  # call rotate_pixels() now
    tasks.if_for_then(no_more_pixels, 1000, show_hours)  # if all pixels are off for 400 millisecond call show_hours()

def no_more_pixels():
    return pixelcount == 0

def show_hours():
    for i in range(hour, hour + 3):
        j = 11 - i % 12
        if (j == 0 or j == 6):
            pass
        elif (j > 6):
            pixels[(j - 2) % len(pixels)] = colors[random.randint(0, len(colors) - 1)]
        else:
            pixels[(j - 1) % len(pixels)] = colors[random.randint(0, len(colors) - 1)]
    pixels.show()
    tasks.after(6000, clear_hours)

def clear_hours():
    pixels.fill((0, 0, 0))
    pixels.show()
    tasks.now(show_minutes)

gc.collect()

while True:
    try:
        pixels.fill((0, 0, 0))
        tasks.now(read_datetime)  # call read_datetime() now
        tasks.now(show_minutes)  # call show_minutes() now
        while True:
            tasks.run()
    except MemoryError:
        tasks.clear()