from math import floor
from cooperativemultitasking import Tasks
from sun import Sun
from pulseio import PWMOut
from servo import Servo
from busio import I2C
from adafruit_pcf8523 import PCF8523
import board
import time

tasks = Tasks()
sun = Sun(50.349929, 7.592469)  # Koblenz
pwm2 = PWMOut(board.A2, frequency = 50)
pwm3 = PWMOut(board.A3, frequency = 50)
servo2 = Servo(pwm2)
servo3 = Servo(pwm3, actuation_range = 90, max_pulse = 1500)
i2c = I2C(board.SCL, board.SDA)
rtc = PCF8523(i2c)
month_to_days = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]

def read_date_time():
    global start, interval
    datetime = rtc.datetime
    print(datetime)
    days = floor(365.25 * (datetime.tm_year - 2018.0))
    days += month_to_days[datetime.tm_mon - 1]
    days += datetime.tm_mday
    days += datetime.tm_hour / 24.0
    days += datetime.tm_min / 1440.0
    start = days
    interval = 0.0
    tasks.after(600000, read_date_time)  # after 10 minutes call read_date_time() again

def track_sun():
    global interval
    pos = sun.position(start + interval)
    print(pos)
    if pos is None:
        servo2.angle = 45
        servo3.angle = 45
    else:
        a, h = pos
        servo2.angle = max(0.0, a - 180.0)
        servo3.angle = h
    interval += 1.0 / 1440.0  # 1 minute in days
    tasks.after(60000, track_sun)  # after 1 minute call track_sun() again

tasks.now(read_date_time)
tasks.after(1000, track_sun)

while True:
    tasks.run()
