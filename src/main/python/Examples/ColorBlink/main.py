import random
import cooperativemultitasking
import board
import adafruit_dotstar

tasks = cooperativemultitasking.Tasks()
led = adafruit_dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, 1)
colors = [(0x28, 0, 0), (0, 0x28, 0), (0, 0, 0x30),
          (0x20, 0x20, 0), (0x20, 0, 0x20), (0, 0x20, 0x20)]

def on():
    led[0] = colors[random.randint(0, len(colors) - 1)]
    tasks.after(1000, off)  # after 1000 milliseconds call off()

def off():
    led[0] = (0, 0, 0)
    tasks.after(2000, on)  # after 1000 milliseconds call on()

tasks.now(on)  # call on() now

while True:
    tasks.run()

