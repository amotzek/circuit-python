from cooperativemultitasking import Tasks
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction
import board

tasks = Tasks()
lightsensor = AnalogIn(board.LIGHT)
led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT

def light():
    led.value = False
    tasks.if_for_then(is_dark, 5000, on)  # if is_dark() for 5 seconds then call on()

def is_dark():
    return lightsensor.value < 7000

def on():
    led.value = True
    tasks.after(1000, off)  # after 1 second call off()

def off():
    led.value = False
    task1 = tasks.after(1000, on)  # after 1 second call on()
    task2 = tasks.if_then(is_light, light)  # if is_light() then call light()
    tasks.only_one_of(task1, task2)  # do either task1 or task2

def is_light():
    return lightsensor.value > 9000

tasks.now(off)  # call off() now

while True:
    tasks.run()

