from cooperativemultitasking import Tasks
from digitalio import DigitalInOut, Direction
import board

tasks = Tasks()
led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT

def on():
    led.value = True
    tasks.after(1000, off)  # after 1000 milliseconds call off()

def off():
    led.value = False
    tasks.after(1000, on)  # after 1000 milliseconds call on()

tasks.now(on)  # call on() now

while True:
    tasks.run()

