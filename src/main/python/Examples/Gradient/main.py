from math import atan2, sqrt, pi
from cooperativemultitasking import Tasks
import board
from busio import I2C
from adafruit_lis3dh import LIS3DH_I2C, RANGE_2_G, STANDARD_GRAVITY
from neopixel import NeoPixel

tasks = Tasks()
pixels = NeoPixel(board.NEOPIXEL, 10, brightness = 0.3)
pixels.fill((0, 0, 0))
pixels.show()
colors = [(32, 0, 0), (32, 32, 0), (0, 32, 0), (0, 0, 32)]
i2c = I2C(board.ACCELEROMETER_SCL, board.ACCELEROMETER_SDA)
lis3dh = LIS3DH_I2C(i2c, address = 25)
lis3dh.range = RANGE_2_G

def polar_acceleration():
    x, y, z = lis3dh.acceleration
    x /= STANDARD_GRAVITY
    y /= -STANDARD_GRAVITY
    phi = atan2(x, y)
    if phi < 0.0:
        phi += 2.0 * pi
    radius = sqrt(x * x + y * y)
    return (phi, radius)

def angular_difference(phi, upsilon):
    delta = upsilon - phi
    return min(abs(delta), abs(2.0 * pi + delta))

def to_intensity(delta):
    return max(0.0, 1.0 - delta * 3.0 / pi)  # Pixel bleibt dunkel, wenn delta groesser als 60 Grad wird

def to_color_index(radius):
    return min(int(radius * len(colors)), len(colors) - 1)  # Farbe wird maximal ab einem Radius von 0.75

def to_color(phi, upsilon, radius):
    intensity = to_intensity(angular_difference(phi, upsilon))
    if intensity == 0.0:
        return (0, 0, 0)
    r, g, b = colors[to_color_index(radius)]
    r = int(r * intensity)
    g = int(g * intensity)
    b = int(b * intensity)
    return (r, g, b)

def set_pixel(i, c):
    if i > 6:
        i -= 2
    else:
        i -= 1
    pixels[i] = c

def show_pixels():
    pixels.show()

def display_polar(phi, radius):
    for i in range(0, 12):
        if not (i == 0 or i == 6):
            set_pixel(i, to_color(phi, i * pi / 6.0, radius))
    show_pixels()

def track():
    phi, radius = polar_acceleration()
    display_polar(phi, radius)
    tasks.after(500, track)

tasks.now(track)

while True:
    tasks.run()
