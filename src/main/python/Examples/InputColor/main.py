import random
import cooperativemultitasking
import board
import supervisor
import adafruit_dotstar

tasks = cooperativemultitasking.Tasks()
led = adafruit_dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, 1)

def on():
    led[0] = color
    tasks.after(3000, off)  # after 3000 milliseconds call off()

def off():
    led[0] = (0, 0, 0)

def receive():
    global color
    color_name = input()
    if color_name == "red":
        color = (0x28, 0, 0)
    elif color_name == "green":
        color = (0, 0x28, 0)
    elif color_name == "blue":
        color = (0, 0, 0x30)
    elif color_name == "yellow":
        color = (0x20, 0x20, 0)
    tasks.now(on)  # call on() now
    tasks.if_then(lambda : supervisor.runtime.serial_bytes_available, receive)  # call receive when input can read

tasks.now(receive)  # call receive() now

while True:
    tasks.run()

