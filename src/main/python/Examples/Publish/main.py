import network
import usocket as socket
import uselect as select
import mqtt
import cooperativemultitasking

ssid_passwords = [('...', '...')]
hostname = '...'
client_id = '...'
user_name = '...'
password = '...'
topic_name = '...'
message_count = 1
broker = None
stream = None

def connect_wlan():
    try:
        access_points = wlan.scan()
        for ssid_password in ssid_passwords:
            ssid = ssid_password[0]
            for access_point in access_points:
                if access_point[0] == bytes(ssid, 'ascii'):
                    wlan.connect(ssid, ssid_password[1])
                    tasks.after(3000, create_socket)
                    return
    except Exception as e:
        print(e)
    tasks.after(30000, connect_wlan)

def create_socket():
    global broker, stream
    try:
        broker = socket.socket()
        address = socket.getaddrinfo(hostname, 1883)[0][-1]
        broker.connect(address)
        stream = broker.makefile('rwb')
        tasks.now(send_connect)
    except Exception as e:
        print(e)
        tasks.after(30000, connect_wlan)

def send_connect():
    try:
        request = mqtt.ConnectRequest(client_id, user_name, password)
        request.write_to(stream)
        tasks.only_one_of(tasks.if_then(can_read_socket, acknowledge_connect),
                          tasks.after(3000, close_socket))
    except Exception as e:
        print(e)
        tasks.now(close_socket)

def can_read_socket():
    return select.select([broker], [], [], 0) is not None

def acknowledge_connect():
    try:
        response = mqtt.AbstractResponse.receive_from(stream)
        if response.connection_accepted():
            tasks.now(publish)
        else:
            print(response.return_code)
            tasks.now(close_socket)
    except Exception as e:
        print(e)
        tasks.now(close_socket)

def publish():
    global message_count
    try:
        print(message_count)
        request = mqtt.PublishRequest(topic_name, str(message_count))
        request.write_to(stream)
        message_count += 1
        tasks.after(30000, publish)
    except Exception as e:
        print(e)
        tasks.now(close_socket)

def close_socket():
    global broker, stream
    broker.close()
    broker = None
    stream = None
    tasks.after(30000, create_socket)

ap = network.WLAN(network.AP_IF)
ap.active(False)
ap = None

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.config(dhcp_hostname = user_name)

tasks = cooperativemultitasking.Tasks()
tasks.now(connect_wlan)

while tasks.available():
    tasks.run()
