import board
from busio import I2C
from adafruit_pcf8523 import PCF8523
import time

i2c = I2C(board.SCL, board.SDA)
rtc = PCF8523(i2c)
rtc.datetime = time.struct_time((2018, 11, 11,  # Jahr, Monat, Tag
                                 12, 10, 0,  # Stunde - 2, Minute, Sekunde
                                 6, 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 11, 1))  # Wochentag, Tag des Jahres, Sommerzeit