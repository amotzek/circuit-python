from cooperativemultitasking import Tasks
from neopixel import NeoPixel
import board

RED = (0x10, 0, 0)

tasks = Tasks()
pixels = NeoPixel(board.NEOPIXEL, 10, brightness = 0.2)
pixels.fill((0, 0, 0))
pixels.show()
pixels[0] = RED

def rotate():
    first = pixels[0]
    for i in range(1, len(pixels)):
        pixels[i - 1] = pixels[i]
    pixels[len(pixels) - 1] = first
    pixels.show()
    tasks.after(300, rotate)  # after 300 milliseconds call rotate() again

tasks.now(rotate)

while True:
    tasks.run()